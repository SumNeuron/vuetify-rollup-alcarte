import Vue from 'vue';
import Vuetify from 'vuetify/lib'

import wrap from '@vue/web-component-wrapper'
import slugify from 'slugify'

import './assets/vuetify.min.css'
import './assets/materialdesignicons.min.css'
import './assets/roboto.css'


Vue.use(Vuetify)

// button using v-btn
import CountButton from './components/CountButton.vue'
// wrapper over vanilla btn to compare / contrast
import WrapBtn from './components/WrapBtn.vue'


// setup for to loop over and register
const components = {
  CountButton, WrapBtn
}


// BEGIN: @vue/web-component-wrapper
// helpers for registering with window
const pascalToKebabHelper = (x, y) => `-${y.toLowerCase()}`
const pascalToKebab = (str) => {
  return str.replace(/\.?([A-Z]+)/g, pascalToKebabHelper).replace(/^-/, "")
}
// define webcomponents
if (typeof window !== 'undefined') {
  Object.keys(components).forEach(name=>{
    let htmlTagName = pascalToKebab(name)
    let vueWebComponent = wrap(Vue, components[name])
    window.customElements.define(htmlTagName, vueWebComponent)
  })
}
// END: @vue/web-component-wrapper


// global register components with Vue
function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });
}

const plugin = { install, }

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
  GlobalVue.use(Vuetify)
}


export const strict = false
// export all components by default
export default components
// ecport each component individually
export {
  CountButton, WrapBtn
}
