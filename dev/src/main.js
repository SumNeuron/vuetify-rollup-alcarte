import Vue from 'vue'
// import App from './App.vue'
import App0 from './App0.vue'
// import CountButton from './components/CountButton.vue'
import store from './store'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

new Vue({
  store,
  vuetify,
  render: h => h(App0)
}).$mount('#app')
