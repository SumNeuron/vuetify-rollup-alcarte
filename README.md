# Description
Trying to rollup the component CountButton (simple wrapper around `VBtn`)
and us it a la carte with all of vuetify's styling.

# How to reproduce:
```
npm i
npm run build:r
```

this will make `pub.min.js` in the `dist/` directory. `dist/demo.html` attempts
to use this component

# How I got here:

1.  have a very simple component, CountButton.vue, which is just a wrapper over the Vuetify component <v-btn>, imported a la carte (see `dev/src/components/CountButton.vue`)
2. Then I configured rollup (`rollup.config.js`) and tried to bundle my "package" (just this component) with `/dev/src/rollup.entry.js`
3. then I rolled up `npm run build:r`
4. then I tried to use the component: (`dist/demo.html`)
