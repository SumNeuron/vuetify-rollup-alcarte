# Vue-Distill

A template for using [Vue][Vue] + [Vuetify][Vuetify] for [Distill][Distill] publications.

## Project structure
The `dev` directory is where you will spend most of your time. Of note are two
files in specific:

1. `dev/public/index.html`
2. `dev/src/main.js`

### `dev/public/index.html`
This file is the boilerplate HTML which will inject your [Vue][Vue] components. The use of [Distill/template][Distill/template] [webcomponents][webcomponents] should occur here _outside_ of any [Vue][Vue] components.
This is to ensure compatibility with their injection script, which may only run
once, and for their benefit would prefer some elements being static HTML.


### `dev/src/main.js`
This file specifies the mounting of [Vue][Vue] components to the HTML file. Each component, unfortunately, must be mounted separately.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


[Vue]: https://vuejs.org/
[Vuetify]:https://vuetifyjs.com/en/
[Distill]:https://distill.pub/
[Distill/template]:https://github.com/distillpub/template
[webcomponents]:https://www.webcomponents.org/
