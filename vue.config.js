module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  pages: {
    index: {
      entry: 'dev/src/main.js',
      template: 'dev/public/index.html',
      filename: 'index.html',
    }
  },
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    },
    mimeTypes: { 'text/html': ['phtml'] }
  }
}
